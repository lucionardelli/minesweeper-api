from django.urls import path

from .views import GameAPIView, GameDetailView

urlpatterns = [
    path('', GameAPIView.as_view(), name='game-list-view'),
    path('<int:pk>/', GameDetailView.as_view(), name='game-detail-view'),
]
