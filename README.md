# Mineswepper REST API #

Develop a REST API for the game of [Minesweeper](https://en.wikipedia.org/wiki/Minesweeper_(video_game))
This is part of a code challenge for Deviget.

## API endpoints

## Online

The api have been deploy in Heroku, and can be found [https://lucio-minesweeper-api.herokuapp.com/api/](https://lucio-minesweeper-api.herokuapp.com/api/)


### Authentication (POST)
For authentication I've chosen JWT (JSON Web Token). It's a standarized
open method for authentication and as suche, there's lot of information
and libraries to work with it.

- api/auth/login/

### List all games (GET)
- api/minesweeper/

### View one game (GET)
- api/minesweeper/{id}

### I want to play! (PUT)
In order to plat, the game needs to be created and
the game's ID need to be known. For details, see [Implementation Notes](#Implementation_Notes).
- api/minesweeper/{id}

### I lost! What can I do? (DELETE)
Deletes a single game using the HTTP method DELETE.

- api/minesweeper/{id}

#### Implementation Notes

API for making a new play on the minesweeper game.
##### Args:
    - row (int): The column of the cell that we want to act onto (zero indexed).
    - column (int): The column of the cell that we want to act onto (zero indexed).
    - sign (char, optional): Defaults to None. Indicates the kind of move intended.
        - If sign is None, it indicates that a cell have been chosen to be revealed.
        - If sign is 'F', it indicates that the cell (row, col) have been flagged.
        - If sign is '?', it indicates that the cell (row, col) have been marked witha a question mark.
        - If sign is '' it indicates that the cell (row, col) have been cleared of any markings.
##### Returns:
    - bool: True if a valid move was made, False otherwise.

#### Online doc
We have a more interactive documentation online:

- api/docs/

## Minimal API Client

As a way of manually testing, an API client is added (implemented in PHP).
It has limited funtionalities and it's clearly not finished but it can be used
to test the overall functionality of the API.


### Client Requirements

The client have been developed in PHP using the `php-curl` library.

```bash
sudo apt install php-curl
```

### Credentials
A handfull user have been created to be able to test the API.

 - username: john
 - password: johndoe

### Implementd functionalities

#### Login
Login with given credentials. The game won't be available otherwhise.
The token is save inside the client so there's no need to store it
externally.

    Method Name:
        login
    Argumnets:
        username: string
        password: string
    Returns:
        String with the authenticated JWT.

#### List all Games
Posibility to get the list of all games.

    Method Name:
        get_games
    Argumnets:
        No arguments
    Returns:
        Array-like object with all the games

#### Get Game
Posibility to retrieve a single game.
The last retrieved game will be set as "current" allowing to make
a play to it.

    Method Name:
        get_game
    Argumnets:
        id: integer
    Returns:
        Array-like object with the specified game

#### New Game
Create and play a new game.
The created game will be set as "current" allowing to make
a play to it.

    Method Name:
        create_game
    Argumnets:
        rows: integer
        columns: integer
        mines: integer
    Returns:
        Array-like object with the created game


#### Start a new Game
Play the internally selected game.

    Method Name:
        play
    Argumnets:
        row: integer
        column: integer
        flag: string (NOT IMPLEMENTED - ignored)
    Returns:
        Array-like object with the specified game
        after making the given cell visible.

## Goals

The development was part of a challenge and the goals where not completly achieved.
The majority of the requirements are implemented but they are all in a development
point. A deep testing, documenting and debugging is due.

- [90%] Design and implement  a documented RESTful API for the game (think of a mobile app for your API)
- [40%] Implement an API client library for the API designed above. Ideally, in a different language, of your preference, to the one used for the API
- [100%] When a cell with no adjacent mines is revealed, all adjacent squares will be revealed (and repeat)
- [50%] Ability to 'flag' a cell with a question mark or red flag
- [95%] Detect when game is over
- [80%] Persistence
- [80%] Time tracking
- [80%] Ability to start a new game and preserve/resume the old ones
- [100%] Ability to select the game parameters: number of rows, columns, and mines
- [60%] Ability to support multiple users/accounts

### Time spent

The estimated dedication in the project was ~6 hours.
