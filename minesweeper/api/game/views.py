from rest_framework import generics
from rest_framework.response import Response
from rest_framework.serializers import ValidationError

from minesweeper.apps.game.models import Game
from minesweeper.libs.permissions import IsCreatorOrReadOnly

from .serializers import GameSerializer, GamePlaySerializer


class GameAPIView(generics.ListCreateAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class GameDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    permission_classes = [IsCreatorOrReadOnly]

    def put(self, request, *args, **kwargs):
        """
        API for making a new play on the minesweeper
        game.
        Args:
            row (int): The first parameter.
            col (int): The second parameter.
            sign (char, optional): Defaults to None. Indicates the
                    kind of move intended.
        Returns:
            bool: True if a valid move was made, False otherwise.

        - If sign is None, it indicates that a cell have
          been chosen to be revealed.
        - If sign is 'F', it indicates that the cell (row, col)
          have been flagged.
        - If sign is '?', it indicates that the cell (row, col)
          have been marked with a question mark.
        - If sign is '' it indicates that the cell (row, col)
          have been cleared of any markings.
        """
        game = self.get_object()
        if game.status == game.PAUSED:
            raise ValidationError("The game is paused!")
        elif game.status in (game.LOST, game.WON):
            raise ValidationError("The game has ended!")

        input_data = request.data
        input_data['game'] = game.pk

        serialized_input = GamePlaySerializer(data=input_data)
        serialized_input.is_valid(raise_exception=True)
        data = serialized_input.data

        game.make_move(data['row'], data['column'], sign=data.get('sign'))

        serializer = GameSerializer(
            game, context={
                'request': request}, many=False)
        return Response(serializer.data)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
