"""Production settings and globals."""
from .common import *  # noqa: F403, F401

from decouple import config
import dj_database_url
import django_heroku

# We're not in Kansas anymore!
DEBUG = False
API_BASE_URL = config('API_BASE_URL')

########## SECRET CONFIGURATION  # noqa: E266
SECRET_KEY = config('SECRET_KEY')
########## END SECRET CONFIGURATION  # noqa: E266

########## HEROKU CONFIGURATION  # noqa: E266
BASE_DIR = dirname(dirname(abspath(__file__)))
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

DATABASES = {'default': dj_database_url.config(conn_max_age=500)}  # noqa: F405

ALLOWED_HOSTS = ('lucio-minesweeper-api.herokuapp.com',)
django_heroku.settings(locals())
########## END HEROKU CONFIGURATION  # noqa: E266


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
