from rest_framework import serializers

from minesweeper.apps.game.models import Game, Cell


class GameSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    game_status = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = [
            'pk',
            'name',
            'user',
            'create_date',
            'end_date',
            'last_action',
            'game_status',
            'rows',
            'columns',
            'mines',
        ]
        read_only_fields = [
            'pk',
            'user',
            'create_date',
            'end_date',
            'last_action',
            'game_status',
        ]

    def get_game_status(self, obj):
        return obj.get_status_display()

    def validate_name(self, value):
        qs = Game.objects.filter(name=value)
        if self.instance:
            qs = qs.exclude(id=self.instance.id)
        if qs.exists():
            raise serializers.ValidationError("Some other game has this name!")
        return value

    def validate_rows(self, rows):
        if rows < 0:
            raise serializers.ValidationError("Number or rows should be positive!")
        return rows

    def validate_columns(self, columns):
        if columns < 0:
            raise serializers.ValidationError("Number or columns should be positive!")
        return columns

    def validate(self, data):
        """
        Check that the specified number of mines is less than the amount of cells.
        """
        if data['mines'] > data['rows'] * data['columns']:
            raise serializers.ValidationError(
                "We can't fit that many mines! "
                "The number if mines should be less than #Rows + #Cols - 1!")
        return data


class GamePlaySerializer(serializers.Serializer):
    game = serializers.PrimaryKeyRelatedField(
            queryset=Game.objects.filter(status=Game.PLAYING).all())
    row = serializers.IntegerField(min_value=0)
    column = serializers.IntegerField(min_value=0)
    sign = serializers.ChoiceField(
            Cell.SIGN_OPTIONS, allow_null=True, allow_blank=True, required=False)

    def validate(self, data):
        """
        Check that the specified number of mines is less than the amount of cells.
        """
        if data['row'] > data['game'].rows or data['column'] > data['game'].columns:
            raise serializers.ValidationError(
                "This game doesn't have the specified cell! Remember they are 0-indexed!")
        return data

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
