from rest_framework import permissions


class IsCreatorOrReadOnly(permissions.BasePermission):
    """
    Full rights granted only to creator
    """
    message = "We are sorry, but this is a private game."

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        user = request.user
        if not user.is_authenticated:
            return False
        if user.is_superuser:
            # Oh! Super user! Of course!
            return True
        return user == obj.user


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
