"""minesweeper URL Configuration"""

from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


from rest_framework_swagger.views import get_swagger_view


urlpatterns = [
    # Admin site
    path('admin/', admin.site.urls),

    # API online documentation
    path('api/docs/', get_swagger_view(title='Lucio - Deviget - Minesweeper API')),
    path('api/docs/auth/', include('rest_framework.urls')),

    # JWT authentication
    path('api/auth/token/obtain/', TokenObtainPairView.as_view(), name='jwt-obtain'),
    path('api/auth/token/refresh/', TokenRefreshView.as_view(), name='jwt-refresh'),

    # API v1 urls
    path('api/v1/minesweeper/', include('minesweeper.api.game.urls')),
]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
