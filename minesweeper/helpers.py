# -*- coding: utf-8 -*-
import random
import string


def gen_random_token(lon, alphabet=None, use_special=False):
    if not alphabet:
        # If not alphabet specified, default to URL safe base64 alphabet.
        # https://tools.ietf.org/html/rfc4648#section-5
        alphabet = string.ascii_uppercase + string.ascii_lowercase + string.digits + '-_'
    if use_special:
        alphabet += '¡!{}[]()@#$%^&=+*)'
    return ''.join(random.SystemRandom().choice(alphabet) for _ in range(lon))

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
