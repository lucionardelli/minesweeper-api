"""Common settings and globals."""


import sys
from datetime import timedelta
import logging
from os.path import abspath, basename, dirname, join, normpath
from ..helpers import gen_random_token

from decouple import config

########## PATH CONFIGURATION  # noqa : E266
# Absolute filesystem path to this Django project directory.
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Site name.
SITE_NAME = basename(DJANGO_ROOT)

# Absolute filesystem path to the top-level project folder.
SITE_ROOT = dirname(DJANGO_ROOT)

# Absolute filesystem path to the secret file which holds this project's
# SECRET_KEY. Will be auto-generated the first time this file is interpreted.
SECRET_FILE = normpath(join(SITE_ROOT, '.minesweeper.SECRETKEY'))

# Add all necessary filesystem paths to our system path so that we can use
# python import statements.
sys.path.append(SITE_ROOT)
sys.path.append(normpath(join(DJANGO_ROOT, 'apps')))
sys.path.append(normpath(join(DJANGO_ROOT, 'libs')))
########## END PATH CONFIGURATION  # noqa : E266


########## DEBUG CONFIGURATION  # noqa : E266
# Disable debugging by default.
DEBUG = False
########## END DEBUG CONFIGURATION  # noqa : E266


########## MANAGER CONFIGURATION  # noqa : E266
# Admin and managers for this project. These people receive private site
# alerts.
ADMINS = (
    ('Lucio Nardelli', 'lucionardelli@gmail.com'),
)

MANAGERS = ADMINS
########## END MANAGER CONFIGURATION  # noqa : E266


########## PASSWORD VALIDATION CONFIGURATION  # noqa : E266
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]
########## END PASSWORD VALIDATION CONFIGURATION  # noqa : E266

########## GENERAL CONFIGURATION  # noqa : E266
ALLOWED_HOSTS = []

TIME_ZONE = 'America/Argentina/Buenos_Aires'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = False
USE_L10N = True
########## END GENERAL CONFIGURATION  # noqa : E266


########## MEDIA CONFIGURATION  # noqa : E266
# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = normpath(join(DJANGO_ROOT, 'media'))

# URL that handles the media served from MEDIA_ROOT.
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION  # noqa : E266


########## STATIC FILE CONFIGURATION  # noqa : E266
STATIC_ROOT = normpath(join(DJANGO_ROOT, 'static'))

# URL prefix for static files.
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
ADMIN_MEDIA_PREFIX = '/static/admin/'

########## END STATIC FILE CONFIGURATION  # noqa : E266


########## TEMPLATE CONFIGURATION  # noqa : E266
_TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'admin_tools.template_loaders.Loader',
)

# Directories to search when loading templates.
_TEMPLATE_DIRS = [
    normpath(join(DJANGO_ROOT, 'templates')),
]
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': _TEMPLATE_DIRS,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
########## END TEMPLATE CONFIGURATION  # noqa : E266


########## MIDDLEWARE CONFIGURATION  # noqa : E266
MIDDLEWARE = (
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'minesweeper.libs.global_request.GlobalRequestMiddleware',
)
########## END MIDDLEWARE CONFIGURATION  # noqa : E266


########## APP CONFIGURATION  # noqa : E266
DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Admin panel and documentation.
    'django.contrib.admin',
    'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    'rest_framework',
)

LOCAL_APPS = (
    'minesweeper.apps.game',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
########## END APP CONFIGURATION  # noqa : E266


########## URL CONFIGURATION  # noqa : E266
APPEND_SLASH = True
ROOT_URLCONF = '%s.urls' % SITE_NAME
LOGIN_URL = '/login'
########## END URL CONFIGURATION  # noqa : E266

########## KEY CONFIGURATION  # noqa : E266
# Try to load the SECRET_KEY from our SECRET_FILE. If that fails, then generate
# a random SECRET_KEY and save it into our SECRET_FILE for future loading. If
# everything fails, then just raise an exception.

SECRET_KEY = config('SECRET_KEY')

if not SECRET_KEY:
    try:
        SECRET_KEY = open(SECRET_FILE).read().strip()
    except IOError:
        try:
            with open(SECRET_FILE, 'w') as f:
                SECRET_KEY = gen_random_token(50, use_special=True)
                f.write(SECRET_KEY)
        except IOError:
            raise Exception('Cannot open file `%s` for writing.' % SECRET_FILE)
########## END KEY CONFIGURATION  # noqa : E266

########## LOG CONFIGURATION  # noqa : E266
LOG_LEVEL = logging.INFO
LOG_FORMAT = '%(asctime)s %(levelname)-8s %(message)s'
LOG_DATEFMT = '%a, %d %b %Y %H:%M:%S'
LOG_FILENAME = 'minesweeper.log'
########## END LOG CONFIGURATION  # noqa : E266

########## REST FRAMEWORK CONFIGURATION  # noqa : E266
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_FILTER_BACKENDS': [
        'rest_framework.filters.OrderingFilter',
        'rest_framework.filters.SearchFilter',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10,
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
}
########## END REST FRAMEWORK CONFIGURATION  # noqa : E266

########## JWT CONFIGURATION  # noqa : E266
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=15),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=2),
    'BLACKLIST_AFTER_ROTATION': True,
}
########## END JWT CONFIGURATION  # noqa : E266

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
