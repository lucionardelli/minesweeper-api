from django.contrib import admin
from . import models


class CellsInline(admin.TabularInline):
    model = models.Cell
    extra = 1


@admin.register(models.Game)
class GameAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'user',
        'status')
    inlines = [CellsInline, ]

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
