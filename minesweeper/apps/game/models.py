import random
import datetime
import itertools

from django.conf import settings
from django.db import models

from rest_framework.reverse import reverse as api_reverse


class Cell(models.Model):
    """ Representation of the cell of a minessweeper game.
        The class works as a container and just holds
        the status of each cell. No logic should be placed
        here.
    """
    NONE = 0
    Q_MARK = 1
    FLAGGED = 2
    SIGN_OPTIONS = [
        (NONE, ''),
        (Q_MARK, '?'),
        (FLAGGED, 'F')
    ]

    mine = models.BooleanField(default=False)
    visible = models.BooleanField(default=False)
    sign = models.IntegerField(choices=SIGN_OPTIONS, default=NONE)
    game = models.ForeignKey(
        'game.Game',
        on_delete=models.CASCADE,
        related_name='cells')
    row = models.IntegerField(db_index=True)
    column = models.IntegerField(db_index=True)

    def __str__(self):
        if self.sign == self.Q_MARK:
            return '?'
        elif self.sign == self.FLAGGED:
            return 'F'
        elif self.visible:
            if self.mine:
                # Visible mine..Ouch!
                return '💥'
            else:
                # Visible non-mine. We will show
                # the number of surrounding mines, if any. Blank otherwise.
                surrounding_number_mines = self.game._count_surrounding_mines(self)
                if surrounding_number_mines > 0:
                    return f'{surrounding_number_mines}'  # noqa: E999
                else:
                    return '0'
        else:
            # We don't have any sign for this unexplored cell
            return '☐'

    class Meta:
        ordering = ['row', 'column']
        verbose_name = 'Cell'
        verbose_name_plural = 'Cells'


class Game(models.Model):
    """ Representation of the minesweeping game.
        It contains the data about a single game such as number of rows,
        columns, number of mines, the status of the game,
        and the list of cells.
    """
    PAUSED = 0
    PLAYING = 1
    LOST = 2
    WON = 3
    GAME_STATUS = [
        (PAUSED, 'Paused'),
        (PLAYING, 'Playing'),
        (LOST, 'Lost'),
        (WON, 'Won'),
    ]

    # General information
    name = models.CharField(max_length=128, default='')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE)
    create_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField(null=True)
    last_action = models.DateTimeField(auto_now=True)
    elapsed_time = models.IntegerField(default=0,
                                       help_text=("Time played (in seconds) since last "
                                                  "time the game was paused."))
    status = models.IntegerField(choices=GAME_STATUS, default=PLAYING)

    # Board data
    rows = models.IntegerField(default=9)
    columns = models.IntegerField(default=9)
    mines = models.IntegerField(default=10)

    @property
    def owner(self):
        return self.user

    @property
    def remaining_mines(self):
        return (self.mines -
                self.cells.filter(mine=True, sign=Cell.FLAGGED).count())

    @property
    def is_solved(self):
        # The game is solved iff all non-mines cells are visible
        total_number_of_non_mines = self.rows * self.columns - self.mines
        number_visible_non_mines = self.cells.filter(mine=False, visible=True).count()
        return number_visible_non_mines == total_number_of_non_mines

    @property
    def played_time(self):
        """ Elapsed played time (i.e. time that the game
            haven't been in pause since the game
            have been created).

            TODO : Consider user's timezone
        """
        played_time = self.elapsed_time
        if self.status == self.PLAYING:
            played_time += (datetime.datetime.now() -
                            self.last_action).total_seconds()
        return played_time

    def as_ascii(self):
        game = [f"""
            Mines: {self.remaining_mines}/{self.mines}
            Status: {self.get_status_display()}
            Time played (in seconds): {self.played_time}
            --------------------------------"""]
        # Default ordering of cell model ensures that the board will display correctly
        for row_idx in range(self.rows):
            game.append('\t'.join(str(cell) for cell in self.cells.filter(row=row_idx)))
        return '\n'.join(game)

    def save(self, *args, **kwargs):
        # Dirty hack in the save method to initialize a game on creation.
        needs_initialization = False
        if not self.pk:
            needs_initialization = True
        super().save(*args, **kwargs)
        if needs_initialization:
            self.__initialize_game()

    def __initialize_game(self):
        """
        Upon creation we need initialize the board game.
        I.e. Create each cell and pseudo-randomly choose the mines location.
        """
        # We use this pseudo-randomized values as indexes for an imaginary array
        # of cells order by row number and column number.
        mines_indexes = random.sample(range(self.rows * self.columns), self.mines)
        for row_idx, row in enumerate(range(self.rows)):
            for col_idx, col in enumerate(range(self.columns)):
                is_mine = ((row_idx * self.columns) + col_idx) in mines_indexes
                self.cells.create(row=row,
                                  column=col,
                                  mine=is_mine)
        if not self.name:
            self.name = f"Automatically generated name {self.pk:05}"
            self.save()

    def __reveal_all_cells(self):
        """ Reveal all cells in (allegedly with one query).
            To be called when the game ends (for good or bad)."""
        for cell in self.cells.all():
            cell.visible = True
        Cell.objects.bulk_update(self.cells.all(), ['visible'])

    def game_won(self):
        """ Changes status of the game to WON GAME.
            Kudos!
        """
        assert self.is_solved,\
            "Something went wrong! We still have mines to defuse"
        self.elapsed_time = self.played_time
        self.end_date = datetime.datetime.now()
        self.status = self.WON
        self.save()
        self.__reveal_all_cells()

    def game_lost(self):
        """ Changes status of the game to LOST GAME.
            Kabooom! Best luck for the next time!
        """
        self.elapsed_time = self.played_time
        self.end_date = datetime.datetime.now()
        self.status = self.LOST
        self.save()
        self.__reveal_all_cells()

    def game_pause(self):
        """ Changes status of the game to PAUSED
            Defusing mines can be exhausting. Get
            some rest and come back!
        """
        if self.status == self.PLAYING:
            self.elapsed_time = self.played_time
            self.status = self.PAUSED
            self.save()

    def _get_surronding_cells(self, cell):
        """ Yields surrounding cells taking care of "borders". """
        for row, col in itertools.product(
                range(cell.row - 1, cell.row + 2),
                range(cell.column - 1, cell.column + 2)):
            if ((row == cell.row and col == cell.column)
                    or row + 1 > self.rows
                    or col + 1 > self.columns
                    or row < 0 or col < 0):
                continue
            yield self.cells.get(row=row, column=col)

    def _count_surrounding_mines(self, cell):
        """ Count the surrounding cells that have mines. """
        return sum(1 for cell in self._get_surronding_cells(cell) if cell.mine)

    def _reveal_cell(self, cell):
        """ Reveals given cell if it is not already visible,
        else no action is taken.
        Every time a cell with no adjacent mines is revealed,
        all adjacent cells will be recursively  revealed
        """
        if not cell.visible:
            cell.visible = True
            cell.save()
            if 0 == self._count_surrounding_mines(cell):
                for nei in self._get_surronding_cells(cell):
                    self._reveal_cell(nei)

    def _activate_cell(self, cell):
        if cell.mine:
            # Ups! You blew up!
            cell.visible = True
            cell.save()
            self.game_lost()
        else:
            # Good "guess"! No bomb!
            self._reveal_cell(cell)
            if self.is_solved:
                self.game_won()

    def _flag_cell(self, cell):
        cell.sign = cell.FLAGGED
        cell.save()
        if self.is_solved:
            self.game_won()

    def _qmark_cell(self, cell):
        cell.sign = cell.Q_MARK
        cell.save()

    def _clear_all_marks(self, cell):
        cell.sign = CELL.NONE
        cell.save()

    def make_move(self, row, col, sign=None):
        """ Make a move in the minesweeper's game.
        - If sign is None, it indicates that a cell have
          been chosen to be revealed.
        - If sign is 'F', it indicates that the cell (row, col)
          have been flagged.
        - If sign is '?', it indicates that the cell (row, col)
          have been marked with a question mark.
        - If sign is '' it indicates that the cell (row, col)
          have been cleared of any markings.

        Args:
            row (int):
                row number where the action will be performed.
            col (int):
                column number where the action will be performed.
            sign (char, optional):
                Defaults to None. Indicates the kind of move intended.
        Returns:
            bool: True if a valid move was made, False otherwise.
        """
        if row < 0 or row >= self.rows or col < 0 or col >= self.columns:
            # Invalid cell!
            return False
        if sign not in [None, 'F', '?', '']:
            # Invalid action!
            return False

        cell = self.cells.get(row=row, column=col)
        if sign is None:
            self._activate_cell(cell)
        elif sign == 'F':
            self._flag_cell(cell)
        elif sign == '?':
            self._qmark_cell(cell)
        elif sign == '':
            self._clear_all_marks(cell)
        return True

    class Meta:
        verbose_name = 'Game'
        verbose_name_plural = 'Games'

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
