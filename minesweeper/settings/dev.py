"""Development settings and globals."""

from .common import *  # noqa: F403

from os.path import join, normpath
from os import environ
from decouple import config

########## DATABASE CONFIGURATION  # noqa: E266
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': config('DATABASE_NAME'),
    }
}
########## END DATABASE CONFIGURATION  # noqa: E266

########## DEBUG CONFIGURATION  # noqa: E266  # noqa: E266
DEBUG = True
########## END DEBUG CONFIGURATION  # noqa: E266

########## LOG CONFIGURATION  # noqa: E266
LOG_LEVEL = logging.DEBUG  # noqa: F405
########## END LOG CONFIGURATION  # noqa: E266

########## CACHE CONFIGURATION  # noqa: E266
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
########## END CACHE CONFIGURATION  # noqa: E266

INSTALLED_APPS += (  # noqa: F405
    'django_extensions',
    'rest_framework_swagger',
)


MEDIA_ROOT = normpath(join(SITE_ROOT, 'minesweeper', 'assets', 'media'))  # noqa: F405
STATIC_ROOT = '/tmp/static'

ALLOWED_HOSTS = ('*')

API_BASE_URL = config('API_BASE_URL')
if not API_BASE_URL:
    API_BASE_URL = 'http://127.0.0.1:8888'

########## SWAGGER CONFIG  # noqa: E266
SWAGGER_SETTINGS = {
        'LOGIN_URL': 'rest_framework:login',
        'LOGOUT_URL': 'rest_framework:logout',
}
########## END SWAGGER CONFIG  # noqa: E266

# vim:expandtab:stmartindent:tabstop=4:softtabstop=4:shiftwidth=4:
