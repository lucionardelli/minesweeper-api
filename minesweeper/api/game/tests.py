from minesweeper.apps.game.models import Game

from rest_framework import status
from rest_framework.test import APITestCase

from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse as api_reverse

User = get_user_model()


class GameAPITestCase(APITestCase):
    def setUp(self):
        user_obj = User(username='testUser', email='test@test.com')
        user_obj.set_password("Passtest")
        user_obj.save()
        self.one_game = Game.objects.create(
            user=user_obj,
            name='Test Game 1',
            rows=9,
            columns=9,
            mines=10,
        )
        self.another_game = Game.objects.create(
            user=user_obj,
            name='Test Game 2',
            rows=5,
            columns=3,
            mines=2,
        )

    def authorize(self, username, password):
        obtain_url = api_reverse('jwt-obtain')
        res = self.client.post(
            obtain_url,
            data={
                'username': username,
                'password': password},
            format='json',
            follow=True)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        # authenticate using the token header on other API
        token = 'Bearer %s' % res.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=token)

    def test_create_game_error_1(self):
        """ Test unauthorized game creation
        """
        data = {
            "name": "Test Create Game",
            "rows": 4,
            "columns": 10,
            "mines": 2}
        url = api_reverse("game-list-view")
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_game_ok(self):
        """ Test a successful create
        """
        # Add credentials
        self.authorize('testUser', 'Passtest')

        url = api_reverse("game-list-view")

        data = {"name": "Test Create Game",
                "rows": 4,
                "columns": 10,
                "mines": 2}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        game_id = response.data['pk']
        url = api_reverse("game-detail-view", args=(game_id,))
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_make_move_error(self):
        # Add credentials
        self.authorize('testUser', 'Passtest')

        url = api_reverse("game-list-view")
        data = {"name": "Test ilegal move",
                "rows": 2,
                "columns": 2,
                "mines": 1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        game_id = response.data['pk']
        url = api_reverse("game-detail-view", args=(game_id,))

        data = {"row": 400, "column": 4}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_make_move_ok(self):
        # Add credentials
        self.authorize('testUser', 'Passtest')

        url = api_reverse("game-list-view")
        data = {"name": "Test ilegal move",
                "rows": 2,
                "columns": 2,
                "mines": 1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        game_id = response.data['pk']
        url = api_reverse("game-detail-view", args=(game_id,))

        data = {"row": 1, "column": 0}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_game_won(self):
        # We will cheat here. Look for all non-mines
        # for the first game created and perform a flawless game!
        game = self.one_game
        non_mines = game.cells.filter(mine=False)

        # Add credentials
        self.authorize('testUser', 'Passtest')
        url = api_reverse("game-detail-view", args=(game.pk,))

        for cell in non_mines:
            data = dict(row=cell.row, column=cell.column)
            response = self.client.put(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            game.refresh_from_db()
            if game.status != Game.PLAYING:
                # Already won!
                break

        game.refresh_from_db()
        self.assertEqual(game.status, Game.WON)

    def test_game_lost(self):
        # Again, we will cheat here.
        # But this time in order to lose!
        game = self.another_game
        mine = game.cells.filter(mine=True).first()

        # Add credentials
        self.authorize('testUser', 'Passtest')
        url = api_reverse("game-detail-view", args=(game.pk,))

        data = dict(row=mine.row, column=mine.column)
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        game.refresh_from_db()
        self.assertEqual(game.status, Game.LOST)
